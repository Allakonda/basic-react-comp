import Profile from './Profile/Profile';
import Settings from './Settings/Settings';
import Users from './Users/Users';

export {
    Profile,
    Settings,
    Users
}