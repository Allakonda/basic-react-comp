import React from 'react';
import Header from '../../components/Header/Header';
import SideBar from '../../components/SideBar/SideBar'
import { GridList, GridListTile } from '@material-ui/core';
import { Profile, Settings, Users } from '../../components/Content'

import './Home.css';


class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            activeComponent: 'profile'
        }
        this.changeActiveComponent = this.changeActiveComponent.bind(this);
    }

    changeActiveComponent(activeComponent){
        this.setState({
            activeComponent: activeComponent
        });
    }

    render() {
        return (
            <div>
                <Header />
                <GridList cols={12} cellHeight={'auto'}>
                    <GridListTile cols={2} style={{ height: '100%', borderRight: '1px solid black' }}>
                        <SideBar activeComponent={this.state.activeComponent} changeActiveComponent={this.changeActiveComponent}  />
                    </GridListTile>
                    <GridListTile cols={10} style={{ padding: 20, textAlign: 'justify', }}>
                        {
                            this.state.activeComponent == 'profile' ? <Profile /> : null
                        }
                        {
                            this.state.activeComponent == 'settings' ? <Settings /> : null
                        }
                        {
                            this.state.activeComponent == 'users' ? <Users /> : null
                        }



                    </GridListTile>
                </GridList>

                {/* header */}
                {/* sidebar */}
                {/* content */}
                {/* profile */}
                {/* users */}
                {/* settings */}


            </div>
        )
    }
}

export default Home;