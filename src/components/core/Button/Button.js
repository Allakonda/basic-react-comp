import React from 'react';
import {Button as MatButton} from '@material-ui/core'

import './Button.css';

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            style : {
                color: 'black',
                height:'70%'
            }
        }
    }

    render() {
        let style = this.state.style;
        style['color'] = this.props.color || style.color;
        return (
            <MatButton style={style} onClick={()=>this.props.onClick(this.props.value)}>
                {this.props.value}
            </MatButton>
        )
    }
}

export default Button;