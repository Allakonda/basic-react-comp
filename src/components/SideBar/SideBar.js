import React from 'react';
import './SideBar.css';
import { Button } from '../core';

const listOfComponents = ['profile', 'users', 'settings'];

class SideBar extends React.Component {

    constructor(props) {
        super(props);
    }

    // function name(params) {

    // }

    // name = () => {

    // }

    render() {
        return (
            <div>
                <h1>SideBar</h1>
                {
                    listOfComponents.map((comp, compIndex) => {
                        return (
                            <span key={compIndex} >
                                <Button color={'red'} style={{backgroundColor: 'black'}} value={comp} onClick={this.props.changeActiveComponent} />
                                <br />
                            </span>
                        )
                    })
                }

                {/* <Button value={'profile'} onClick={this.props.changeActiveComponent} />
                <Button value={'users'} onClick={this.props.changeActiveComponent} />
                <Button value={'settings'} onClick={this.props.changeActiveComponent} /> */}

                {/* <button onClick={() => this.props.changeActiveComponent('profile')} className="btn">
                    Profile
                </button>
                <br />
                <button onClick={() => this.props.changeActiveComponent('users')} className="btn">
                    Users
                </button>
                <br />
                <button onClick={() => this.props.changeActiveComponent('settings')} className="btn">
                    Settings
                </button> */}
            </div>
        )
    }
}

export default SideBar;